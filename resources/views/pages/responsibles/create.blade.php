@extends('layouts.master')

@section('title',  'Nouveau Responsables')

@section('head')
    <link href="{{ asset('assets/vendor/select2/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <section class="admin-content">
        <div class="bg-dark m-b-30">
            <div class="container">
                <div class="row p-b-60 p-t-60">
                    <div class="col-md-8 m-auto text-white p-b-30">
                        <h1>Ajouter un nouveau Responsable</h1>
                    </div>
                    <div class="col-md-4 m-auto text-white p-b-30">
                        <div class="text-md-right">
                            <a href="{{ route('responsibles.index') }}" class="btn btn-success"> <i class="mdi mdi-arrow-left-bold-circle"></i> Annuler</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container  pull-up">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body ">
                            <form action="{{ route('responsibles.store') }}" method="POST">
                                @csrf
                                @include('layouts.partials._formResponsibles', ['submitButtonText' => 'Ajouter'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
