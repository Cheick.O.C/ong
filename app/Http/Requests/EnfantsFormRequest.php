<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnfantsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'first_name'    => 'required',
        'last_name' => 'required',
        'born_date' => 'required',
        'arrive_at' => 'required',
        'email' => 'email|nullable',
        'phone' => 'nullable',
        'school'    => 'nullable',
        'schooling' => 'nullable',
        'school_location'   => 'nullable',
        'family_situation'  => 'required',
        'description'   => 'required|min:6',
        'adress'   => 'required|min:3',
        'age'   => 'required|max:2',
        'image' => 'required|image',
        'proffession'   => 'required',
        'responsible_id'    => 'required|exists:responsibles,id',
        'location' => 'required|max:80|min:5',
        'father_name' => 'required|max:80|min:5',
        'mother_name' => 'required|max:80|min:5',
        'proffession_father' => 'required|max:80|min:5',
        'proffession_mother' => 'required|max:80|min:5',
        ];
    }
}
