<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnfantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enfants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->date('born_date')->nullable();
            $table->date('arrive_at');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('adress');
            $table->string('school')->nullable();
            $table->string('school_location')->nullable();
            $table->string('family_situation');
            $table->string('location');
            $table->string('father_name');
            $table->string('mother_name');
            $table->string('proffession_father');
            $table->string('proffession_mother');
            
            $table->string('description')->nullable();
            $table->string('image')->nullable();
            $table->string('schooling');
            $table->string('proffession');
            $table->string('age');
            $table->unsignedBigInteger('responsible_id')->default(0);
            $table->unsignedBigInteger('user_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enfants');
    }
}
