<div class="form-row">
    <div class="form-group col-md-6">
        <label for="first_name">Prénom</label>
        <input type="text" name="first_name" class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}" id="first_name" value="{{ old('first_name')?? $enfant->first_name }}" placeholder="Ex : Amadou" >

        @error ('first_name')
        <div class="text-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="last_name">Nom</label>
        <input type="text" name="last_name" class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}" id="last_name" value="{{ old('last_name')?? $enfant->last_name }}" placeholder="Ex : Coulibaly">

        @error ('last_name')
                <div class="text-danger">
                    {{ $message }}
                </div>
        @enderror
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="father_name">Nom du Père</label>
        <input type="text" name="father_name" class="form-control {{ $errors->has('father_name') ? 'is-invalid' : '' }}" id="father_name" value="{{ old('father_name')?? $enfant->father_name }}" placeholder="Ex : Amadou Sylla" >

        @error ('father_name')
        <div class="text-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="mother_name">Nom de la Mère</label>
        <input type="text" name="mother_name" class="form-control {{ $errors->has('mother_name') ? 'is-invalid' : '' }}" id="mother_name" value="{{ old('mother_name')?? $enfant->mother_name }}" placeholder="Ex : Wassa Coulibaly">

        @error ('mother_name')
                <div class="text-danger">
                    {{ $message }}
                </div>
        @enderror
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="proffession_father">Proffession du Père</label>
        <input type="text" name="proffession_father" class="form-control {{ $errors->has('proffession_father') ? 'is-invalid' : '' }}" id="proffession_father" value="{{ old('proffession_father')?? $enfant->proffession_father }}" placeholder="Ex : Menusier" >

        @error ('proffession_father')
        <div class="text-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="proffession_mother">Proffession de la Mère</label>
        <input type="text" name="proffession_mother" class="form-control {{ $errors->has('proffession_mother') ? 'is-invalid' : '' }}" id="mother_name" value="{{ old('proffession_mother')?? $enfant->proffession_mother }}" placeholder="Ex : Commerçante">

        @error ('proffession_mother')
                <div class="text-danger">
                    {{ $message }}
                </div>
        @enderror
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="phone">Numéro de téléphone</label>
        <input type="text" name="phone" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" id="phone" value="{{ old('phone') ?? $enfant->phone }}" placeholder="Ex : 74859625">

        @error ('phone')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group col-md-6">
        <label for="email">Email</label>
        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" value="{{ old('email') ?? $enfant->email }}" placeholder="Ex : nom@gmail.com">

        @error ('email')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="school">Ecole</label>
        <input type="text" name="school" class="form-control {{ $errors->has('school') ? 'is-invalid' : '' }}" id="school" value="{{ old('school')?? $enfant->school }}" placeholder="nom de l'école" >

        @error ('school')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group col-md-6">
        <label for="schooling">Scolarité</label>
        <select id="schooling" class="font-secondary form-control {{ $errors->has('schooling') ? 'is-invalid' : '' }} js-select2"  name="schooling">
            <option value="Primaire">Primaire</option>
            <option value="Secondaire">Secondaire</option>
            <option value="Supérieur">Supérieur</option>
            <option value="Formation">Formation</option>
            <option value="Université">Université</option>
            <option value="Autre">Autre</option>
        </select>

        @error ('schooling')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="born_date">Date de naissance</label>
        <input type="date" name="born_date" class="form-control {{ $errors->has('born_date') ? 'is-invalid' : '' }}" id="born_date" value="{{ old('born_date')?? $enfant->born_date }}" selected>

        @error ('born_date')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group col-md-6">
        <label for="arrive_at">Date date d'arrivée</label>
        <input type="date" name="arrive_at" class="form-control {{ $errors->has('arrive_at') ? 'is-invalid' : '' }}" id="arrive_at" value="{{ old('arrive_at') ?? $enfant->arrive_at }}">

        @error ('arrive_at')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="family_situation">Situation familliale</label>
        <select id="family_situation" class="font-secondary form-control {{ $errors->has('family_situation') ? 'is-invalid' : '' }} js-select2"  name="family_situation">
            <option value="Orphelin de père">Orphelin de père</option>
            <option value="Orphelin de mère">Orphelin de mère</option>
            <option value="Orphelin de père et de mère">Orphelin de père et de mère</option>
            <option value="A une mère">A une mère</option>
            <option value="A un père">A un père</option>
            <option value="A ses deux parents">A ses deux parents</option>
        </select>

        @error ('family_situation')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="school_location">Lacalité de l'école</label>
        <input type="text" name="school_location" class="form-control {{ $errors->has('school_location') ? 'is-invalid' : '' }}" id="school_location" value="{{ old('school_location') ?? $enfant->school_location }}" placeholder="">

        @error ('school_location')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="description">Description</label>
       <textarea name="description" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" id="description" placeholder="bref description de l'enfant" cols="30" rows="2">{{ old('description')?? $enfant->description }}</textarea>

        @error ('description')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group col-md-6">
        <label for="proffession">Proffession</label>
        <input type="text" name="proffession" class="form-control {{ $errors->has('proffession') ? 'is-invalid' : '' }}" id="proffession" value="{{ old('proffession') ?? $enfant->proffession }}" placeholder="Ex : nom@gmail.com">

        @error ('proffession')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="age">Age</label>
        <input type="text" name="age" class="form-control {{ $errors->has('age') ? 'is-invalid' : '' }}" id="age" value="{{ old('age') ?? $enfant->age }}" placeholder="Ex: 12 ans" >

        @error ('age')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="responsible_id">Responsable en charge de l'enfant</label>
        <select id="responsible_id" class="font-secondary form-control {{ $errors->has('responsible_id') ? 'is-invalid' : '' }} js-select2"  name="responsible_id">
            <option value="choisissez le responsable de l'enfant">--choisissez le responsable de l'enfant--</option>
            @foreach($responsibles as $responsible)
                <option value="{{ $responsible->id }}">{{ $responsible->fullName() }}</option>
            @endforeach
        </select>

        @error ('responsible_id')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="adress">Adresse</label>
        <input type="text" name="adress" class="form-control {{ $errors->has('adress') ? 'is-invalid' : '' }}" id="adress" value="{{ old('adress') ?? $enfant->adress }}" placeholder="Ex : ville quartier porte rue">

        @error ('adress')
            <div class="text-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group col-md-6">
                <label for="image">Image</label>
                <input type="file" name="image" class="form-control {{ $errors->has('image') ? 'is-invalid' : '' }}" id="image" value="{{ old('image') ?? $enfant->image }}">
        
                @error ('image')
                    <div class="text-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="location">Foyer</label>
            <select name="location" id="location" class="form-control {{ $errors->has('location') ? 'is-invalid' : '' }}">
                <option value="En famille D'acceuil">En foyer</option>
                <option value="A l'orphélina">A l'orphelina</option>
            </select>

            @error ('location')
                <div class="text-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>



<input type="submit" value="{{ $submitButtonText }}" class="btn btn-primary">
