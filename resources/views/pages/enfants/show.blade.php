@extends('layouts.master')

@section('title',  $enfant->fullName())

@section('head')
    <link href="{{ asset('assets/vendor/select2/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <section class="admin-content">
        <div class="bg-dark m-b-30">
            <div class="container">
                <div class="row p-b-60 p-t-60">
                    <div class="col-md-8 m-auto text-white p-b-30">
                        <h1>{{ $enfant->fullName() }}</h1>
                        <p>
                            @if($enfant->image)
                            <img src="{{ asset('storage/'.$enfant->image) }}" alt="L'image de l'enfant" width="200" height="100">
                            @endif
                        </p>
                    </div>
                    
                    <div class="col-md-4 m-auto text-white p-b-30">
                        <div class="text-md-right">
                            <a href="{{ route('enfants.edit', $enfant) }}" class="btn btn-info"> <i class="mdi mdi-account-edit"></i> Modifier</a>
                            <!-- <a href="{{ route('enfants.edit', $enfant) }}" class="btn btn-info"> <i class="mdi mdi-account-edit"></i> Extrait de naissance</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container  pull-up">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body ">
                            <form>
                            <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="first_name">Prénom</label>
                                        <input type="text" class="form-control" id="first_name" value="{{ $enfant->first_name }}" disabled>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="last_name">Nom</label>
                                        <input type="text" class="form-control" id="last_name" value="{{ $enfant->last_name }}" disabled>
                                    </div>
                                </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="father_name">Nom du Père</label>
                                    <input type="text" name="father_name" class="form-control" id="father_name" value="{{ $enfant->father_name }}" disabled>

                                    @error ('father_name')
                                    <div class="text-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="mother_name">Nom de la Mère</label>
                                    <input type="text" name="mother_name" class="form-control" id="mother_name" value="{{ $enfant->mother_name }}" disabled>

                                    @error ('mother_name')
                                            <div class="text-danger">
                                                {{ $message }}
                                            </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="proffession_father">Proffession du Père</label>
                                    <input type="text" name="proffession_father" class="form-control" id="proffession_father" value="{{ $enfant->proffession_father }}" disabled>

                                    @error ('proffession_father')
                                    <div class="text-danger">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="proffession_mother">Proffession de la Mère</label>
                                    <input type="text" name="proffession_mother" class="form-control" id="mother_name" value="{{ $enfant->proffession_mother }}" disabled>

                                    @error ('proffession_mother')
                                            <div class="text-danger">
                                                {{ $message }}
                                            </div>
                                    @enderror
                                </div>
                            </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="phone">Numéro de téléphone</label>
                                        <input type="text" class="form-control" id="phone" value="{{ $enfant->phone }}" disabled>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control" id="email" value="{{ $enfant->email }}" disabled>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="school">Ecole</label>
                                        <input type="text" class="form-control" id="school" value="{{ $enfant->school }}" disabled>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="schooling">Scolarité</label>
                                        <input type="text" class="form-control" id="schooling" value="{{ $enfant->schooling }}" disabled>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="born_date">Date de naissance</label>
                                        <input type="text" class="form-control" id="born_date" value="{{ $enfant->born_date }}" disabled>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="arrive_at">Date date d'arrivée</label>
                                        <input type="text" class="form-control" id="arrive_at" value="{{ $enfant->arrive_at->format('d/m/Y') }}" disabled>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="family_situation">Situation familliale</label>
                                        <input type="text" class="form-control" value="{{ $enfant->family_situation }}" disabled>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="school_location">Lacalité de l'école</label>
                                        <input type="text" class="form-control" id="school_location" value="{{ $enfant->school_location }}" disabled>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="description">Description</label>
                                    <textarea class="form-control" id="description" cols="30" rows="2" disabled>{{ $enfant->description }}</textarea>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="proffession">Proffession</label>
                                        <input type="text" class="form-control" id="proffession" value="{{ $enfant->proffession }}" disabled>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="age">Age</label>
                                        <input type="text" name="age" class="form-control" id="age" value="{{ $enfant->age }}" disabled>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="responsable_id">Responsable en charge de l'enfant</label>
                                        <input type="text" class="form-control" id="responsable_id" value="{{ $enfant->responsible->fullName() }}" disabled>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="adress">Adresse</label>
                                        <input type="text" name="adress" class="form-control" id="adress" value="{{ $enfant->adress }}" disabled>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="location">Localité de l'enfant</label>
                                        <input type="text" name="location" class="form-control" id="location" value="{{ $enfant->location }}" disabled>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-danger" value="Supprimer">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
