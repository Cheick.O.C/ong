<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResponsiblesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3|max:80',
            'last_name' => 'required|min:3|max:80',
            'email' => 'required|email',
            'phone' => 'required|digits:8|unique:responsibles,phone',
            'profil' => 'required',
            'adress' => 'required|min:5',
        ];
    }
}
