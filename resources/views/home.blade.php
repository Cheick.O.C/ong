@extends('layouts.master')

@section('title', 'Tableau de bord')

@section('content')
    <section class="admin-content">
        <div class="container p-t-20">
            <div class="row">
                <div class="col-12 m-b-30">
                    <h1>Tableau de bord</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 m-b-30">
                    <!--widget card begin-->
                    <div class="card m-b-30">
                        <div class="card-body">
                            <div class="row p-t-20 p-b-20">
                                <div class="my-auto col-md-7">

                                    <h1 class="m-0">{{ number_format(count(App\Models\Enfant::all()), 0, ",", " ") }}</h1>
                                    <p class="m-0 text-muted">Enfants</p>
                                </div>
                                <div class="my-auto text-md-right col-md-5">
                                    <a href="#" class="btn btn-rounded-circle btn-lg btn-white text-dark">
                                    <i class="icon-placeholder mdi mdi-human-child"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--widget card ends-->
                </div>

                <div class="col-lg-6 m-b-30">
                    <!--widget card begin-->
                    <div class="card m-b-30">
                        <div class="card-body">
                            <div class="row p-t-20 p-b-20">
                                <div class="my-auto col-md-7">

                                    <h1 class="m-0">{{ number_format(count(App\Models\Responsible::all()), 0, ",", " ") }}</h1>
                                    <p class="m-0 text-muted">Responsables</p>
                                </div>
                                <div class="my-auto text-md-right col-md-5">
                                    <a href="#" class="btn btn-rounded-circle btn-lg btn-white text-dark">
                                    <i class="icon-placeholder mdi mdi-teach"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--widget card ends-->
                </div>

                <div class="col-lg-6 m-b-30">
                    <!--widget card begin-->
                    <div class="card m-b-30">
                        <div class="card-body">
                            <div class="row p-t-20 p-b-20">
                                <div class="my-auto col-md-7">

                                    <h1 class="m-0">{{ number_format(count(App\Models\Enfant::all()), 0, ",", " ") }}</h1>
                                    <p class="m-0 text-muted">Enfants inscris</p>
                                </div>
                                <div class="my-auto text-md-right col-md-5">
                                    <a href="#" class="btn btn-rounded-circle btn-lg btn-white text-dark">
                                        <i class="mdi mdi-bank"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--widget card ends-->
                </div>
            </div>
        </div>
    </section>
@endsection
