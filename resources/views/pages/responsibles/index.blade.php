@extends('layouts.master')

@section('title', 'Responsables')
@section('content')
    <section class="admin-content">
        <div class="bg-dark m-b-30">
            <div class="container">
                <div class="row p-b-60 p-t-60">

                    <div class="col-md-8 m-auto text-white p-b-30">
                        <h1>Responsables</h1>
                    </div>

                    <div class="col-md-4 m-auto text-white p-b-30">
                        <div class="text-md-right">
                            <a href="{{ route('responsibles.create') }}" class="btn btn-success"> <i class="mdi mdi-plus"></i> Nouveau Responsable</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pull-up">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive p-t-10">
                                <table id="example" class="table   " style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Email</th>
                                            <th>Téléphone</th>
                                            <th>Profil</th>
                                            <th>Adresse</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($responsibles as $responsible)
                                            <tr>
                                                <td>
                                                    {{	$responsible->fullName()  }}
                                                </td> 
                                                <td>
                                                    {{ $responsible->email }}
                                                </td>
                                                <td>
                                                    {{ $responsible->phone }}
                                                </td>
                                                <td>
                                                    {{ $responsible->profil }}
                                                </td>
                                                <td>
                                                    {{ $responsible->adress }}
                                                </td>
                                                <td>
                                                    <a href="{{ route('responsibles.show',$responsible) }}" class="btn  btn-primary">Voir</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Email</th>
                                            <th>Téléphone</th>
                                            <th>Profil</th>
                                            <th>Adresse</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row justify-content-center">
                <nav aria-label="">
                    {{-- {{ $Responsibles->links() }} --}}
                </nav>
            </div>
        </div>
    </section>
@endsection
