<!DOCTYPE html>
    <html lang="fr">

    @include('layouts.partials.head')

    <body class="sidebar-pinned ">
        @include('layouts.partials.sidebar')

        <main class="admin-main">
        <!--site header begins-->

            @include('layouts.partials.header')

        <!--site header ends -->
            @if(Session::get('notification.message'))
                <div class="alerte alerte-{{ Session::get('notification.type')}}">
                    Session::get('notification.message')
                </div>
            @endif
            @yield('content')
        </main>


        @include('layouts.partials.modals.month')

        <script src="{{ asset('assets/js/theme.min.js') }}"></script>
        <script src="{{ asset('assets/js/dashboard-01.js') }}"></script>
        @yield('script')
        {{-- <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script> --}}

        @if (session()->has('notification.message'))
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
            <script type="text/javascript">
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 5000
                });

                Toast.fire({
                    type: "{{ session()->get('notification.type') }}",
                    title: "{{ session()->get('notification.message') }}"
                });
            </script>
        @endif
    </body>
</html>
