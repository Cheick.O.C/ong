<?php

use App\Http\Controllers\EnfantsController;
use App\Http\Controllers\ResponsiblesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/extraits/{enfant}', [EnfantsController::class,'ActeDeNaissance'])->name('extraits');
Route::get('/listPrimaire', [EnfantsController::class,'LesEnfantsEnPrimaire'])->name('primary');
Route::get('/listOrphelina', [EnfantsController::class,'LesEnfantsDansOrphelina'])->name('orphelina');
Route::get('/listFoyer', [EnfantsController::class,'LesEnfantsEnFoyer'])->name('foyer');

Route::resource('/enfants', EnfantsController::class);
Route::resource('/responsibles', ResponsiblesController::class);
