@extends('layouts.master')

@section('title', 'Enfants')
@section('content')
    <section class="admin-content">
        <div class="bg-dark m-b-30">
            <div class="container">
                <div class="row p-b-60 p-t-60">

                    <div class="col-md-8 m-auto text-white p-b-30">
                        <h1>Enfants</h1>
                    </div>

                    <div class="col-md-4 m-auto text-white p-b-30">
                        <div class="text-md-right ml-2">
                            <a href="{{ route('enfants.create') }}" class="btn btn-success"> <i class="mdi mdi-plus"></i> Nouveau Enfant</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pull-up">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive p-t-10">
                                <table id="example" class="table   " style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Date d'arrivé'</th> 
                                            <th>Scolarité</th>
                                            <th>age</th>
                                            <th>responsable</th>     
                                            <th>description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($enfants as $enfant)
                                            <tr>
                                                <td>
                                                    {{ $enfant->fullname() }}
                                                </td>
                                                <td>
                                                    {{ $enfant->arrive_at->format('d/m/Y') }}
                                                </td>  
                                                <td>
                                                    {{ $enfant->schooling }}
                                                </td> 
                                                <td>
                                                    {{ $enfant->age }}
                                                </td>
                                                <td>
                                                    {{ $enfant->responsible->fullName() }}
                                                </td> 
                                                <td>
                                                    {{ $enfant->description }}
                                                </td>
                                                <td class="hidden-sm hidden-xs">
                                                    <div class="btn-group btn-group-xs pull-right">
                                                        <button class="btn btn-warning dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Actions <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                                            <li><a class="dropdown-item" href="{{ route('enfants.show', $enfant) }}"> Voir</a></li>
                                                            <li><a class="dropdown-item" href="{{ route('extraits', $enfant) }}">Extrait de naissance</a></li>
                                                            <li><a class="dropdown-item" href="{{ route('primary') }}">Les enfants en primaire</a></li>
                                                            <li><a class="dropdown-item" href="{{ route('orphelina') }}">Les enfants présentent dans l'ophélina</a></li>
                                                            <li><a class="dropdown-item" href="{{ route('foyer') }}">Les enfants en famille d'acceuil</a></li>
                                                        </ul>
                                                    </div>   
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Nom</th>
                                            <th>Date d'arrivé</th> 
                                            <th>Scolarité</th>
                                            <th>age</th>
                                            <th>responsable</th>     
                                            <th>description</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <nav aria-label="">
                    {{ $enfants->links() }}
                </nav>
            </div>
        </div> 
    </section>
@endsection
