<header class="admin-header">
    <a href="#" class="sidebar-toggle" data-toggleclass="sidebar-open" data-target="body"> </a>

    <nav class="ml-auto">
        <ul class="nav align-items-center">
            <li class="nav-item dropdown ">
                <div class="dropdown-menu  dropdown-menu-right">
                    {{-- <a class="dropdown-item" href="{{ route('profile.index') }}">Mon Profil</a> --}}
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="">Déconnexion</a>
                </div>
            </li>
        </ul>
    </nav>
</header>
