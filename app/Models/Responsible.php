<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Responsible extends Model
{
    use HasFactory;
    protected $fillable = ['first_name', 'last_name', 'phone', 'email','adress','profil'];

    public function fullName(){
        return $this->first_name.' '.$this->last_name;
    }

    public function enfants(){
        return $this->hasMany('App\Models\Enfant');
    }
}
