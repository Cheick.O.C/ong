<div class="form-row">
  <div class="form-group col-md-6">
    <label for="firstname">Prénom</label>
    <input type="text" name="firstname" class="form-control {{ $errors->has('firstname') ? 'is-invalid' : '' }}" id="firstname" value="{{ old('firstname') ?? $user->firstname }}" placeholder="Ex : " autofocus>
    {!! $errors->first('firstname', '<span class="help-block invalid-feedback">:message</span>') !!}
  </div>
  <div class="form-group col-md-6">
    <label for="lastname">Nom</label>
    <input type="text" name="lastname" class="form-control {{ $errors->has('lastname') ? 'is-invalid' : '' }}" id="lastname" value="{{ old('lastname') ?? $user->lastname }}" placeholder="Ex : Coulibaly">
    {!! $errors->first('lastname', '<span class="help-block invalid-feedback">:message</span>') !!}
  </div>
</div>

<div class="form-row">
  <div class="form-group col-md-6">
    <label for="phone">Téléphone</label>
    <input type="text" name="phone" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" id="phone" value="{{ old('phone') ?? $user->phone }}" placeholder="">
    {!! $errors->first('phone', '<span class="help-block invalid-feedback">:message</span>') !!}
  </div>
  <div class="form-group col-md-6">
    <label for="post">Email</label>
    <input type="text" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" value="{{ old('email') ?? $user->email }}" placeholder="">
    {!! $errors->first('category_id', '<span class="help-block invalid-feedback">:message</span>') !!}
  </div>
</div>
<div class="form-row">
  <div class="form-group col-md-6">
    <label for="password">Mot de passe</label>
    <input type="text" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" id="password" value="{{ old('password') ?? $user->password }}" placeholder="">
    {!! $errors->first('password', '<span class="help-block invalid-feedback">:message</span>') !!}
  </div>
</div>
<div class="form-group">
  <input type="submit" value="{{ $submitButtonText }}" class=" btn btn-primary btn-block">
</div>
