<div class="form-row">
    <div class="form-group col-md-6">
        <label for="firstname">Prénom</label>
        <input type="text" name="first_name" class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}" id="firstname" value="{{ old('first_name') ?? $responsible->first_name }}" placeholder="Ex : Mala" autofocus>

        @error ('first_name')
            <span class="help-block invalid-feedback">{{ $message }}</span>
        @enderror
    </div>

    <div class="form-group col-md-6">
        <label for="last_name">Nom</label>
        <input type="text" name="last_name" class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}" id="lastname" value="{{ old('lastname') ?? $responsible->last_name }}" placeholder="Ex : Traoré">

        @error ('last_name')
            <span class="help-block invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="phone">Téléphone</label>
        <input type="text" name="phone" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" id="phone" value="{{ old('phone') ?? $responsible->responsible }}" placeholder="Ex : 90354569" autofocus>

        @error ('phone')
            <span class="help-block invalid-feedback">{{ $message }}</span>
        @enderror
    </div>

    <div class="form-group col-md-6">
        <label class="form-control-label" for="email">Email</label>
        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" value="{{ old('email') ?? $responsible->email }}" placeholder="Ex : Titulaire@gmail.com" >
        @error ('email')
            <span class="help-block invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="profil">Profil</label>
        <select name="profil" class="form-control" id="profil">
            <option value="">---Profil---</option>
            <option value="Orphélina">Orphélina</option>
            <option value="Foyer d'acceuil">Foyer d'acceuil</option>
        </select>

        @error ('profil')
            <span class="help-block invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="adress">Adresse</label>
        <input type="text" name="adress" class="form-control {{ $errors->has('adress') ? 'is-invalid' : '' }}" id="adress" value="{{ old('adress') ?? $responsible->adress }}" placeholder="Ex: ville, quartier, rue, porte" autofocus>

        @error ('adress')
            <span class="help-block invalid-feedback">{{ $message }}</span>
        @enderror
    </div>
</div>

<input type="submit" value="{{ $submitButtonText }}" class=" btn btn-primary">
