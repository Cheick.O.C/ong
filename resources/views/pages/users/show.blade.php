@extends('layouts.master')

@section('title',  $user->firstname.' '.$user->lastname)

@section('head')
    <link href="{{ asset('assets/vendor/select2/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <section class="admin-content">
        <div class="bg-dark m-b-30">
            <div class="container">
                <div class="row p-b-60 p-t-60">
                    <div class="col-md-8 m-auto text-white p-b-30">
                        <h1>{{ $user->firstname.' '.$user->lastname }}</h1>
                    </div>
                    <div class="col-md-4 m-auto text-white p-b-30">
                        <div class="text-md-right">
                            <a href="{{ route('users.edit', $user) }}" class="btn btn-info"> <i class="mdi mdi-account-edit"></i> Modifier</a>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <div class="container  pull-up">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body ">
                            <form method="POST" action="{{ route('users.destroy',$user) }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="firstname">Prénom</label>
                                    <input type="text" name="firstname" class="form-control {{ $errors->has('firstname') ? 'is-invalid' : '' }}" id="firstname" value="{{ $user->firstname }}" placeholder="Ex : " autofocus disabled>
                                    {!! $errors->first('firstname', '<span class="help-block invalid-feedback">:message</span>') !!}
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="lastname">Nom</label>
                                    <input type="text" name="lastname" class="form-control {{ $errors->has('lastname') ? 'is-invalid' : '' }}" id="lastname" value="{{ $user->lastname }}" placeholder="Ex : " disabled>
                                    {!! $errors->first('lastname', '<span class="help-block invalid-feedback">:message</span>') !!}
                                  </div>
                                </div>

                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="phone">Téléphone</label>
                                    <input type="text" name="phone" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" id="phone" value="{{ $user->phone }}" placeholder="" disabled>
                                    {!! $errors->first('phone', '<span class="help-block invalid-feedback">:message</span>') !!}
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="post">Email</label>
                                    <input type="text" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" value="{{ $user->email }}" placeholder="" disabled>
                                    {!! $errors->first('category_id', '<span class="help-block invalid-feedback">:message</span>') !!}
                                  </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="password">Mot de passe</label>
                                    <input type="text" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" id="password" value="{{ $user->password }}" placeholder="" disabled>
                                    {!! $errors->first('password', '<span class="help-block invalid-feedback">:message</span>') !!}
                                  </div>
                                </div>
                                 
                                <input type="submit" class="btn btn-danger btn-block" value="Supprimer">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
