<aside class="admin-sidebar">
    <div class="admin-sidebar-brand">
        <!-- begin sidebar branding-->
        <img class="admin-brand-logo" src="{{ asset('assets/img/ONG_LOGO.JPG') }}" width="80" alt="{{ config('app.name') }}">
        <!-- end sidebar branding-->
        <div class="ml-auto">
            <!-- sidebar pin-->
            <a href="#" class="admin-pin-sidebar btn-ghost btn btn-rounded-circle"></a>
            <!-- sidebar close for mobile device-->
            <a href="#" class="admin-close-sidebar"></a>
        </div>
    </div>
    <div class="admin-sidebar-wrapper js-scrollbar">
        <ul class="menu">
            <li class="menu-item {{ (Route::currentRouteName() == 'home') ? 'active' : '' }}">
                <a href="{{ route('home') }}" class="menu-link">
                    <span class="menu-label">
                        <span class="menu-name">
                            Tableau de bord
                        </span>
                    </span>

                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-view-dashboard"></i>
                    </span>
                </a>
            </li>

            <li class="menu-item {{ (Route::currentRouteName() == 'enfants*') ? 'active' : '' }}">
                <a href="{{ route('enfants.index') }}" class="menu-link">
                    <span class="menu-label">
                        <span class="menu-name">
                            Enfants
                        </span>
                    </span>

                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-human-child"></i>
                    </span>
                </a>
            </li>

            <li class="menu-item {{ (Route::currentRouteName() == 'responsibles*') ? 'active' : '' }}">
                <a href="{{ route('responsibles.index') }}" class="menu-link">
                    <span class="menu-label">
                        <span class="menu-name">
                            Responsables
                        </span>
                    </span>

                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-teach"></i>
                    </span>
                </a>
            </li> 

            <li class="menu-item {{ (Route::currentRouteName() == 'denhres*') ? 'active' : '' }}">
                <a href="#" class="menu-link">
                    <span class="menu-label">
                        <span class="menu-name">
                            Denhrées
                        </span>
                    </span>

                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-food"></i>
                    </span>
                </a>
            </li>

            {{-- <li class="menu-item {{ (Route::currentRouteName() == 'groups.index') ? 'active' : '' }}">
                <a href="{{ route('groups.index') }}" class="menu-link">
                    <span class="menu-label">
                        <span class="menu-name">
                            Groupes
                        </span>
                    </span>

                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-account-group"></i>
                    </span>
                </a>
            </li>

            <li class="menu-item {{ (Route::currentRouteName() == 'messages.index') ? 'active' : '' }}">
                <a href="{{ route('messages.index') }}" class="menu-link">
                    <span class="menu-label">
                        <span class="menu-name">
                            Messages
                        </span>
                    </span>

                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-message"></i>
                    </span>
                </a>
            </li>

            <li class="menu-item">
                <a href="{{ route('logout') }}" class="menu-link">
                    <span class="menu-label">
                        <span class="menu-name">
                            Déconnexion
                        </span>
                    </span>

                    <span class="menu-icon">
                        <i class="icon-placeholder mdi mdi-logout"></i>
                    </span>
                </a>
            </li>--}}
        </ul>
    </div>
</aside>
