@extends('layouts.master')

@section('title',  $responsible->first_name.' '.$responsible->last_name)

@section('head')
    <link href="{{ asset('assets/vendor/select2/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
    <section class="admin-content">
        <div class="bg-dark m-b-30">
            <div class="container">
                <div class="row p-b-60 p-t-60">
                    <div class="col-md-8 m-auto text-white p-b-30">
                        <h1>{{ $responsible->first_name.' '.$responsible->last_name }}</h1>
                    </div>
                    <div class="col-md-4 m-auto text-white p-b-30">
                        <div class="text-md-right">
                            <a href="{{ route('responsibles.edit', $responsible) }}" class="btn btn-info"> <i class="mdi mdi-account-edit"></i> Modifier</a>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <div class="container  pull-up">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body ">
                            <form method="POST" action="{{ route('responsibles.destroy',$responsible) }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="first_name">Prénom</label>
                                    <input type="text" name="first_name" class="form-control" id="first_name" value="{{ $responsible->first_name }}" disabled>
                                    @error('first_name')
                                        <span class="help-block invalidFeedback">{{ $message }}</span>
                                    @enderror
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="last_name">Nom</label>
                                    <input type="text" name="last_name" class="form-control" id="last_name" value="{{ $responsible->last_name }}" disabled>
                                    @error('las_tname')
                                        <span class="help-block invalidFeedback">{{ $message }}</span>
                                    @enderror
                                  </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input type="text" name="email" class="form-control" id="email" value="{{ $responsible->email }}" disabled>
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="phone">Téléphone</label>
                                    <input type="text" name="phone" class="form-control" id="phone" value="{{ $responsible->phone }}" disabled>
                                  </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="profil">Profil</label>
                                        <input type="text" name="profil" class="form-control" id="profil" value="{{ $responsible->profil }}" disabled>    
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="adress">Adresse</label>
                                        <input type="text" name="adress" class="form-control" id="adress" value="{{ $responsible->adress }}" disabled>
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-danger btn-block" value="Supprimer">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
