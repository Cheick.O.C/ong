<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enfant extends Model
{
    use HasFactory;
    protected $fillable = [
        'first_name',
        'last_name',
        'born_date',
        'arrive_at',
        'email',
        'phone',
        'school',
        'schooling',
        'school_location',
        'family_situation',
        'description',
        'adress',
        'age',
        'image',
        'proffession',
        'responsible_id',
        'location',
        'father_name',
        'mother_name',
        'proffession_father',
        'proffession_mother',
    ];
    protected $dates =  [
        'born_date',
        'arrive_at',
    ];
    
    public function fullName(){
        return $this->first_name.' '.$this->last_name;
    }

    public function responsible(){
        return $this->belongsTo('App\Models\Responsible');
    }
}
