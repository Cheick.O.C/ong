@extends('layouts.master')

@section('title',  'Liste des enfants')

@section('head')
    <link href="{{ asset('assets/vendor/select2/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
<section class="admin-content">
        <div class="bg-dark m-b-30">
            <div class="container">
                <div class="row p-b-60 p-t-60 ">
                <h1 class="text-white">{{ $enfant->fullName()}}</h1>
                </div>
            </div>
        </div>

<div class="container pull-up">
    <div class="row">
        <div class="col-8">
            <!--widget card begin-->
            <div class="card m-5-b-30">
                <div class="card-header bg-light">
                    <div class="card-header d-flex justify-content-between">
                        <div class="col-md-6 m-auto text-dark p-b-5">
                            <h4>District de Bamako</h4>
                            <p><img src="{{asset('assets/img/RepubliqueDuMali.jpg')}}" alt="district de bamako" width="100" height="100" class="rounded"></p>
                            <h5>Mairie de la commune III</h5>
                        </div>

                        <div class="col-md-6 m-auto text-dark p-b-5 p-top text-center">
                            <h4>République du Mali</h4>
                            <p>Un Peuple - Un But - Une Foi</p>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <h5 class="m-5">Centre principale de la commune III :</h5>
                    <h4 class="text-center">Extrait de Naissancence : </h4>
                    <li class="list-group-item"><strong>Nom : </strong>{{ $enfant->first_name }} </li>
                    <li class="list-group-item"><strong>Prénom : </strong>{{ $enfant->last_name }}</li>
                    <li class="list-group-item"><strong>Sexe : </strong>{{ $enfant->last_name }}                        </li>
                    <li class="list-group-item"><strong>Proffession : </strong>{{ $enfant->proffession }}</li>
                    <li class="list-group-item"><strong>Date et lieu de Naissance : </strong> {{ $enfant->born_date }}</li>
                    <li class="list-group-item"><strong>Fils de : </strong>{{ $enfant->father_name }}</li>
                    <li class="list-group-item"><strong>Et de : </strong> {{ $enfant->mother_name }}</li>
                    <li class="list-group-item"><strong>Proffession du Père : </strong> {{ $enfant->proffession_father }}</li>
                    <li class="list-group-item"><strong>Proffession de la mère : </strong> {{ $enfant->proffession_mother }}</li>
                </ul>
                <div class="bg-light">
                    <p class="text-center pt-3">Etablit le {{ Now()->format('d/m/Y') }} au centre principale de la commune III</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection