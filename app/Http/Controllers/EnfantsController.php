<?php

namespace App\Http\Controllers;

use App\Http\Requests\EnfantsFormRequest;
use App\Models\Enfant;
use App\Models\Responsible;
use Illuminate\Http\Request;

class EnfantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enfants = Enfant::paginate(15);
        return view('pages.enfants.index', compact('enfants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $enfant = new Enfant();
        $responsibles = Responsible::all();
        return view('pages.enfants.create', compact('enfant','responsibles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EnfantsFormRequest $request)
    {
        // if($request->hasFile('image')){
        //     $image = $request->image;
        //     $extension = $image->getClientOriginalExtension(); // get image extension
        //     $filename = time().'.'.$extension;
        //     $image->move('uploads/categories/', $filename);
        // }
        $enfant = Enfant::create([
        'first_name'    => $request->first_name,
        'last_name' => $request->last_name,
        'born_date' => $request->born_date,
        'arrive_at' => $request->arrive_at,
        'email' => $request->email,
        'phone' => $request->phone,
        'school'    => $request->school,
        'schooling' => $request->schooling,
        'school_location'   => $request->school_location,
        'family_situation'  => $request->family_situation,
        'description'   => $request->description,
        'adress'   => $request->adress,
        'age'   => $request->age,
        'image' => $request->image,
        'proffession'   => $request->proffession,
        'responsible_id'    => $request->responsible_id,
        'location' => $request->location,
        'father_name' => $request->father_name,
        'mother_name' => $request->mother_name,
        'proffession_father' => $request->proffession_father,
        'proffession_mother' => $request->proffession_mother,
        ]);

        $this->storeImage($enfant);

        return redirect()->route('enfants.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Enfant  $enfant
     * @return \Illuminate\Http\Response
     */
    public function show(Enfant $enfant)
    {
        $responsibles = Responsible::all();
        return view('pages.enfants.show', compact('enfant','responsibles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Enfant  $enfant
     * @return \Illuminate\Http\Response
     */
    public function edit(Enfant $enfant)
    {
        $responsibles = Responsible::all();
        return view('pages.enfants.edit',compact('enfant','responsibles') );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Enfant  $enfant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enfant $enfant)
    {
        $enfant->update([
            'first_name'    => $request->first_name,
            'last_name' => $request->last_name,
            'born_date' => $request->born_date,
            'arrive_at' => $request->arrive_at,
            'email' => $request->email,
            'phone' => $request->phone,
            'school'    => $request->school,
            'schooling' => $request->schooling,
            'school_location'   => $request->school_location,
            'family_situation'  => $request->family_situation,
            'description'   => $request->description,
            'adress'   => $request->adress,
            'age'   => $request->age,
            'image' => $request->image,
            'proffession'   => $request->proffession,
            'responsible_id'    => $request->responsible_id,
            'location' => $request->location,
            'father_name' => $request->father_name,
            'mother_name' => $request->mother_name,
            'proffession_father' => $request->proffession_father,
            'proffession_mother' => $request->proffession_mother,
            ]);
            $this->storeImage($enfant);
            

            return redirect()->route('enfants.show',compact('enfant'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Enfant  $enfant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enfant $enfant)
    {
        $enfant->delete();

        return redirect()->route('enfants.index');
    }
    
    public function ActeDeNaissance(Enfant $enfant)
    {
        return view('pages.enfants.acteDeNaissance',compact('enfant'));

    }

    private function storeImage(Enfant $enfant)
    {
        if(request('image')){
            $enfant->update([
                'image' => request('image')->store('Avatars','public')
            ]) ;
        }
    }

    public function LesEnfantsDansOrphelina()
    {
        $enfants = Enfant::where('location','A l\'orphélina')->paginate(15);
        return view('pages.enfants.index', compact('enfants'));
    }

    public function LesEnfantsEnFoyer()
    {
        $enfants = Enfant::where('location','En famille D\'acceuil')->paginate(15);
        return view('pages.enfants.index', compact('enfants'));
    }

    public function LesEnfantsEnPrimaire()
    {
        $enfants = Enfant::where('schooling','Primaire')->paginate(15);
        return view('pages.enfants.index', compact('enfants'));
    }
    
}
