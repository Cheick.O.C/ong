<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResponsiblesFormRequest;
use App\Models\Responsible;
use Illuminate\Http\Request;

class ResponsiblesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responsibles = Responsible::paginate(15);
       return view('pages.responsibles.index', compact('responsibles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responsible = new Responsible();
        return view( 'pages.responsibles.create', compact('responsible') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ResponsiblesFormRequest $request)
    {
        Responsible::create([
            'first_name'    => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'adress' => $request->adress,
            'profil' => $request->profil,
        ]);

        return redirect()->route('responsibles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\responsible  $responsible
     * @return \Illuminate\Http\Response
     */
    public function show(Responsible $responsible)
    {
        return view( 'pages.responsibles.show', compact('responsible') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\responsible  $responsible
     * @return \Illuminate\Http\Response
     */
    public function edit(Responsible $responsible)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\responsible  $responsible
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Responsible $responsible)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\responsible  $responsible
     * @return \Illuminate\Http\Response
     */
    public function destroy(Responsible $responsible)
    {
        //
    }
}
